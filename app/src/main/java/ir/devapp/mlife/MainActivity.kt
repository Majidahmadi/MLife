package ir.devapp.mlife

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import ir.devapp.mlife.model.ModelRequest

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnMainSubmit: Button = findViewById(R.id.btn_main_submit)
        val btnMainCancel: Button = findViewById(R.id.btn_main_cancel)
        val edtMainType: EditText = findViewById(R.id.edt_main_type)
        val edtMainName: EditText = findViewById(R.id.edt_main_name)
        val edtMainInfo: EditText = findViewById(R.id.edt_main_info)
        val txvMainShow: TextView = findViewById(R.id.txv_main_show)

        btnMainSubmit.setOnClickListener {
            var name: String = edtMainName.text.toString()
            var type: String = edtMainType.text.toString()
            var info: String = edtMainInfo.text.toString()
            val model: ModelRequest = ModelRequest()
            model.adItems(type, name, info)
            model.save()
            Toast.makeText(this, "Hello World...", Toast.LENGTH_SHORT).show()
        }

        btnMainCancel.setOnClickListener {
            var model: ModelRequest = ModelRequest.random
            txvMainShow.text = model.mainName + " - " + model.mainInfo + " - " + model.mainType
        }
    }
}
