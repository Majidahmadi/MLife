package ir.devapp.mlife

import android.app.Application
import com.activeandroid.ActiveAndroid
import com.activeandroid.Cache.getContext
import com.activeandroid.Configuration

open class DatabaseTestCase : Application() {

    val DB_NAME = "test.db"


    fun setUp() {
        val configuration = Configuration.Builder(getContext())
                .setDatabaseName(DB_NAME)
                ?.setDatabaseVersion(1)
                ?.create()

        ActiveAndroid.initialize(configuration)
    }

    fun tearDown() {
        getContext()?.deleteDatabase(DB_NAME)
        ActiveAndroid.dispose()
    }

}