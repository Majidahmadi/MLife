package ir.devapp.mlife.model

import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table
import com.activeandroid.query.Select

@Table(name = "model_request")
open class ModelRequest : Model {

    @Column(name = "main_type")
    var mainType: String? = null

    @Column(name = "main_name")
    var mainName: String? = null

    @Column(name = "main_info")
    var mainInfo: String? = null

    val all: List<ModelRequest>
        get() = getMany(ModelRequest::class.java, "name")

    constructor() : super() {}

    constructor(main_type: String, main_name: String, main_info: String) {
        this.mainType = main_type
        this.mainName = main_name
        this.mainInfo = main_info
    }

    companion object {
        val random: ModelRequest
            get() = Select().from(ModelRequest::class.java).executeSingle()

        val mainName: String
            get() {
                val ModelRequest = Select().from(ModelRequest::class.java).executeSingle<ModelRequest>()
                return ir.devapp.mlife.model.ModelRequest.mainName
            }

        val driverInfo: ModelRequest
            get() = Select().from(ModelRequest::class.java).executeSingle()

    }

    fun adItems(main_type: String, main_name: String, main_info: String) {
        this.mainType = main_type
        this.mainName = main_name
        this.mainInfo = main_info
    }

}